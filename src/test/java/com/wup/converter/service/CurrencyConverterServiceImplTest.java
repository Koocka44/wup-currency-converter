package com.wup.converter.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wup.converter.dao.CurrencyConverterDao;
import com.wup.converter.dao.entity.CurrencyEntity;
import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.CurrencyRate;
import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.model.RateType;

class CurrencyConverterServiceImplTest {

	private static final double EUR_RATE = 1;

	private static final double USD_SELL_RATE = 4;

	private static final double USD_BUY_RATE = 2;

	private static final double HUF_SELL_RATE = 8;

	private static final double HUF_BUY_RATE = 5;
	
	private static final double AMOUNT_TO_CONVERT = 10;

	@Mock
	private CurrencyConverterDao currencyConverterDao;

	@InjectMocks
	private CurrencyConverterServiceImpl underTest;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetRatesByCurrencyCallsDaoCorrectly() {
		CurrencyCode input = CurrencyCode.CAD;
		setUpDao();
		underTest.getRatesByCurrency(input, RateType.SELL);
		verify(currencyConverterDao, times(1)).findCurrencyByCode(input);
		verify(currencyConverterDao, times(1)).findOtherCurrenciesByCode(input);
	}

	@Test
	void testGetRatesByCurrencyCalculatesBuyRatesCorrectly() {
		setUpDao();
		RateResult expected = createBuyRateResult();

		RateResult actual = underTest.getRatesByCurrency(CurrencyCode.EUR, RateType.BUY);

		assertEquals(actual, expected);
	}
	
	@Test
	void testGetRatesByCurrencyCalculatesSellRatesCorrectly() {
		setUpDao();
		RateResult expected = createSellRateResult();

		RateResult actual = underTest.getRatesByCurrency(CurrencyCode.EUR, RateType.SELL);

		assertEquals(actual, expected);
	}

	@Test
	void testExchangeCurrenciesInBuyRate() {
		CurrencyEntity currencyHuf = new CurrencyEntity(2L, CurrencyCode.HUF, HUF_BUY_RATE, HUF_SELL_RATE);
		CurrencyEntity currencyUsd = new CurrencyEntity(3L, CurrencyCode.USD, USD_BUY_RATE, USD_SELL_RATE);
		ExchangeResult expected = createBuyExchangeResult();
		when(currencyConverterDao.findCurrencyByCode(CurrencyCode.HUF)).thenReturn(currencyHuf);
		when(currencyConverterDao.findCurrencyByCode(CurrencyCode.USD)).thenReturn(currencyUsd);
		
		ExchangeResult actual = underTest.exchangeCurrency(CurrencyCode.HUF, AMOUNT_TO_CONVERT, CurrencyCode.USD, RateType.BUY);
		
		assertEquals(actual, expected);
		
	}
	
	@Test
	void testExchangeCurrenciesInSellRate() {
		CurrencyEntity currencyHuf = new CurrencyEntity(2L, CurrencyCode.HUF, HUF_BUY_RATE, HUF_SELL_RATE);
		CurrencyEntity currencyUsd = new CurrencyEntity(3L, CurrencyCode.USD, USD_BUY_RATE, USD_SELL_RATE);
		ExchangeResult expected = createSellExchangeResult();
		when(currencyConverterDao.findCurrencyByCode(CurrencyCode.HUF)).thenReturn(currencyHuf);
		when(currencyConverterDao.findCurrencyByCode(CurrencyCode.USD)).thenReturn(currencyUsd);
		
		ExchangeResult actual = underTest.exchangeCurrency(CurrencyCode.HUF, AMOUNT_TO_CONVERT, CurrencyCode.USD, RateType.SELL);
		
		assertEquals(actual, expected);
		
	}

	private ExchangeResult createBuyExchangeResult() {
		return new ExchangeResult(CurrencyCode.HUF, CurrencyCode.USD, AMOUNT_TO_CONVERT, (AMOUNT_TO_CONVERT / HUF_BUY_RATE) * USD_BUY_RATE);
	}
	
	private ExchangeResult createSellExchangeResult() {
		return new ExchangeResult(CurrencyCode.HUF, CurrencyCode.USD, AMOUNT_TO_CONVERT, (AMOUNT_TO_CONVERT / HUF_SELL_RATE) * USD_SELL_RATE);
	}

	private RateResult createBuyRateResult() {
		List<CurrencyRate> rates = new ArrayList<>();
		rates.add(new CurrencyRate(CurrencyCode.HUF, EUR_RATE / HUF_BUY_RATE));
		rates.add(new CurrencyRate(CurrencyCode.USD, EUR_RATE / USD_BUY_RATE));
		return new RateResult(CurrencyCode.EUR, rates);
	}
	
	private RateResult createSellRateResult() {
		List<CurrencyRate> rates = new ArrayList<>();
		rates.add(new CurrencyRate(CurrencyCode.HUF, EUR_RATE / HUF_SELL_RATE));
		rates.add(new CurrencyRate(CurrencyCode.USD, EUR_RATE / USD_SELL_RATE));
		return new RateResult(CurrencyCode.EUR, rates);
	}

	private void setUpDao() {
		CurrencyEntity currentCurrency = new CurrencyEntity(1L, CurrencyCode.EUR, EUR_RATE, EUR_RATE);
		CurrencyEntity otherCurrencyHuf = new CurrencyEntity(2L, CurrencyCode.HUF, HUF_BUY_RATE, HUF_SELL_RATE);
		CurrencyEntity otherCurrencyUsd = new CurrencyEntity(3L, CurrencyCode.USD, USD_BUY_RATE, USD_SELL_RATE);
		when(currencyConverterDao.findCurrencyByCode(BDDMockito.any(CurrencyCode.class))).thenReturn(currentCurrency);
		when(currencyConverterDao.findOtherCurrenciesByCode(BDDMockito.any(CurrencyCode.class))).thenReturn(Arrays.asList(otherCurrencyHuf, otherCurrencyUsd));
	}

}
