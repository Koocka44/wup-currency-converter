package com.wup.converter.web.facade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.model.RateType;
import com.wup.converter.service.CurrencyConverterService;
import com.wup.converter.web.request.ExchangeRequest;
import com.wup.converter.web.request.RateRequest;

class CurrencyConverterFacadeTest {

	@Mock
	private CurrencyConverterService currencyConverterService;
	
	@InjectMocks
	private CurrencyConverterFacade underTest;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void testGetRatesByCurrencyCallsServiceWithInputData() {
		CurrencyCode currencyCode = CurrencyCode.EUR;
		RateType rateType = RateType.BUY;
		RateRequest request = new RateRequest(currencyCode, rateType);
		
		underTest.getRatesByCurrency(request);
		
		verify(currencyConverterService, times(1)).getRatesByCurrency(request.getCurrencyCode(), request.getRateType());
	}
	
	@Test
	void testGetRatesByCurrencyReturnsReturnedData() {
		RateResult expected = new RateResult();
		CurrencyCode currencyCode = CurrencyCode.EUR;
		RateType rateType = RateType.BUY;
		RateRequest request = new RateRequest(currencyCode, rateType);
		when(currencyConverterService.getRatesByCurrency(BDDMockito.any(CurrencyCode.class),BDDMockito.any(RateType.class))).thenReturn(expected);
		
		RateResult actual = underTest.getRatesByCurrency(request);
		
		assertEquals(expected, actual);
	}

	@Test
	void testExchangeCurrencyCallsServiceWithInputData() {
		CurrencyCode sourceCurrencyCode = CurrencyCode.EUR;
		CurrencyCode targetCurrencyCode = CurrencyCode.USD;
		double amount = 100;
		RateType rateType = RateType.SELL;
		ExchangeRequest request = new ExchangeRequest(sourceCurrencyCode, amount, targetCurrencyCode, rateType);
		
		underTest.exchangeCurrency(request);
		
		verify(currencyConverterService, times(1)).exchangeCurrency(request.getSourceCurrencyCode(), request.getAmount(), request.getTargetCurrencyCode(), request.getRateType());
	}
	
	@Test
	void testExchangeCurrencyReturnsReturnedData() {
		ExchangeResult expected = new ExchangeResult();
		CurrencyCode sourceCurrencyCode = CurrencyCode.EUR;
		CurrencyCode targetCurrencyCode = CurrencyCode.USD;
		double amount = 100;
		RateType rateType = RateType.SELL;
		ExchangeRequest request = new ExchangeRequest(sourceCurrencyCode, amount, targetCurrencyCode, rateType);
		when(currencyConverterService.exchangeCurrency(BDDMockito.any(CurrencyCode.class), BDDMockito.anyDouble(), BDDMockito.any(CurrencyCode.class), BDDMockito.any(RateType.class))).thenReturn(expected);
		
		ExchangeResult actual = underTest.exchangeCurrency(request);
		
		assertEquals(expected, actual);
	}

}
