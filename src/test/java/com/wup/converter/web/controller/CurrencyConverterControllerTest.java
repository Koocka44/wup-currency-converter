package com.wup.converter.web.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.model.RateType;
import com.wup.converter.web.facade.CurrencyConverterFacade;
import com.wup.converter.web.request.ExchangeRequest;
import com.wup.converter.web.request.RateRequest;

class CurrencyConverterControllerTest {

	@Mock
	private CurrencyConverterFacade currencyConverterFacade;

	@InjectMocks
	private CurrencyConverterController underTest;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetRatesByCurrencyCallsFacadeWithInputData() {
		CurrencyCode currencyCode = CurrencyCode.EUR;
		RateType rateType = RateType.BUY;
		RateRequest request = new RateRequest(currencyCode, rateType);
		BindingResult br = mock(BindingResult.class);

		underTest.getRatesByCurrency(request, br);

		verify(currencyConverterFacade, times(1)).getRatesByCurrency(request);
	}

	@Test
	void testGetRatesByCurrencyReturnsReturnedData() {
		RateResult rateResult = new RateResult();
		BindingResult br = mock(BindingResult.class);
		when(currencyConverterFacade.getRatesByCurrency(BDDMockito.any(RateRequest.class))).thenReturn(rateResult);

		ResponseEntity<RateResult> response = underTest.getRatesByCurrency(new RateRequest(), br);

		assertEquals(rateResult, response.getBody());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	void testExchangeCurrencyCallsFacadeWithInputData() {
		CurrencyCode sourceCurrencyCode = CurrencyCode.EUR;
		CurrencyCode targetCurrencyCode = CurrencyCode.USD;
		double amount = 100;
		RateType rateType = RateType.SELL;
		BindingResult br = mock(BindingResult.class);
		ExchangeRequest request = new ExchangeRequest(sourceCurrencyCode, amount, targetCurrencyCode, rateType);

		underTest.exchangeCurrency(request, br);

		verify(currencyConverterFacade, times(1)).exchangeCurrency(request);
	}

	@Test
	void testExchangeCurrencyReturnsReturnedData() {
		ExchangeResult result = new ExchangeResult();
		BindingResult br = mock(BindingResult.class);
		when(currencyConverterFacade.exchangeCurrency(BDDMockito.any(ExchangeRequest.class))).thenReturn(result);

		ResponseEntity<ExchangeResult> response = underTest.exchangeCurrency(new ExchangeRequest(), br);

		assertEquals(result, response.getBody());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

}
