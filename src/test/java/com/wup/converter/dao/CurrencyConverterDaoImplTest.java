package com.wup.converter.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wup.converter.dao.entity.CurrencyEntity;
import com.wup.converter.dao.repository.CurrencyRepository;
import com.wup.converter.model.CurrencyCode;

class CurrencyConverterDaoImplTest {

	@Mock
	private CurrencyRepository currencyRepository;

	@InjectMocks
	private CurrencyConverterDaoImpl underTest;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testFindCurrencyByCodeCallsRepositoryWithCurrencyCode() {
		CurrencyCode currencyCode = CurrencyCode.HUF;

		underTest.findCurrencyByCode(currencyCode);

		verify(currencyRepository, times(1)).findByCurrencyCode(currencyCode);
	}
	
	@Test
	void testFindCurrencyByCodeReturnsValueReturnedByRepository() {
		CurrencyEntity expected = new CurrencyEntity();
		when(currencyRepository.findByCurrencyCode(BDDMockito.any(CurrencyCode.class))).thenReturn(expected);
		
		CurrencyEntity actual = underTest.findCurrencyByCode(CurrencyCode.HUF);

		assertEquals(actual, expected);
	}

	@Test
	void testFindOtherCurrenciesByCodeCallsRepositoryWithCurrencyCode() {
		CurrencyCode currencyCode = CurrencyCode.HUF;

		underTest.findOtherCurrenciesByCode(currencyCode);

		verify(currencyRepository, times(1)).findByCurrencyCodeNot(currencyCode);
	}
	
	@Test
	void testFindOtherCurrenciesByCodeReturnsValueReturnedByRepository() {
		List<CurrencyEntity> expected = Arrays.asList(new CurrencyEntity(), new CurrencyEntity());
		when(currencyRepository.findByCurrencyCodeNot(BDDMockito.any(CurrencyCode.class))).thenReturn(expected);
		
		List<CurrencyEntity> actual = underTest.findOtherCurrenciesByCode(CurrencyCode.HUF);

		assertEquals(actual, expected);
	}

}
