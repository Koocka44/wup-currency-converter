package com.wup.converter.service;

import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.model.RateType;

public interface CurrencyConverterService {
	
	/**
	* Returns the exchange rate of one currency to the other currencies handled by the system.
	*
	* @param  currencyCode the source currency to get the rates for
	* @param  rateType the rate type to calculate with ( buy or sell )
	* @return the input information and the input currency's rate in other currencies.
	*/
	RateResult getRatesByCurrency(CurrencyCode currencyCode, RateType rateType);

	/**
	* Returns the exchange rate of one currency to the other currencies handled by the system.
	*
	* @param  sourceCurrencyCode the source currency to change from
	* @param  amount the amount of money in the source currency to change 
	* @param  targetCurrencyCode the target currency to change to
	* @param  rateType the rate type to calculate with ( buy or sell )
	* @return the input information and the source amount exchanged to the target currency.
	*/
	ExchangeResult exchangeCurrency(CurrencyCode sourceCurrencyCode, double amount, CurrencyCode targetCurrencyCode,
			RateType rateType);
}
