package com.wup.converter.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wup.converter.dao.CurrencyConverterDao;
import com.wup.converter.dao.entity.CurrencyEntity;
import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.CurrencyRate;
import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.model.RateType;

@Service
public class CurrencyConverterServiceImpl implements CurrencyConverterService {

	@Autowired
	private CurrencyConverterDao currencyConverterDao;

	@Override
	public RateResult getRatesByCurrency(final CurrencyCode currencyCode, final RateType rateType) {
		CurrencyEntity currentCurrency = currencyConverterDao.findCurrencyByCode(currencyCode);
		double rateInBase = getRateToBaseCurrency(currentCurrency, rateType);
		List<CurrencyEntity> otherCurrencies = currencyConverterDao.findOtherCurrenciesByCode(currencyCode);
		List<CurrencyRate> rates = getOtherCurrencyRates(rateType, rateInBase, otherCurrencies);
		return new RateResult(currencyCode, rates);
	}

	@Override
	public ExchangeResult exchangeCurrency(final CurrencyCode sourceCurrencyCode, final double amount, final CurrencyCode targetCurrencyCode, final RateType rateType) {
		CurrencyEntity sourceCurrency = currencyConverterDao.findCurrencyByCode(sourceCurrencyCode);
		CurrencyEntity targetCurrency = currencyConverterDao.findCurrencyByCode(targetCurrencyCode);
		double targetAmount = calculateTargetAmount(amount, rateType, sourceCurrency, targetCurrency);
		return new ExchangeResult(sourceCurrencyCode, targetCurrencyCode, amount, targetAmount);
	}

	private double calculateTargetAmount(final double amount, final RateType rateType, final CurrencyEntity sourceCurrency, final CurrencyEntity targetCurrency) {
		double surceValueInBase = amount / getRateToBaseCurrency(sourceCurrency, rateType);
		double targetRate = getRateToBaseCurrency(targetCurrency, rateType);
		double targetAmount = targetRate * surceValueInBase;
		return targetAmount;
	}

	private List<CurrencyRate> getOtherCurrencyRates(final RateType rateType, double valueInBase, final List<CurrencyEntity> otherCurrencies) {
		List<CurrencyRate> rates = new ArrayList<>();
		otherCurrencies.forEach(currency -> {
			rates.add(createRateResult(rateType, valueInBase, currency));
		});
		return rates;
	}

	private CurrencyRate createRateResult(final RateType rateType, double valueInBase, final CurrencyEntity currency) {
		double rateInBase = getRateToBaseCurrency(currency, rateType);
		return new CurrencyRate(currency.getCurrencyCode(), valueInBase / rateInBase );
	}

	private double getRateToBaseCurrency(CurrencyEntity currentCurrency, RateType rateType) {
		double valueInBase = currentCurrency.getBuyRate();
		if (rateType.equals(RateType.SELL)) {
			valueInBase = currentCurrency.getSellRate();
		}
		return valueInBase;
	}
}
