package com.wup.converter.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wup.converter.dao.entity.CurrencyEntity;
import com.wup.converter.model.CurrencyCode;

@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long>{

	/**
	* Get the currency of the given currency code.
	*
	* @param  currencyCode the currency code to search for.
	* @return the data of the given currency from the database.
	*/
	CurrencyEntity findByCurrencyCode(CurrencyCode code);

	/**
	* Get all the currencies except the one with the given currency code.
	*
	* @param  currencyCode the currency code to NOT search for.
	* @return the data of all the currencies except the one with the given currency code from the database.
	*/
	List<CurrencyEntity> findByCurrencyCodeNot(CurrencyCode currencyCode);

}
