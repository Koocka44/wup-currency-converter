package com.wup.converter.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wup.converter.dao.entity.CurrencyEntity;
import com.wup.converter.dao.repository.CurrencyRepository;
import com.wup.converter.model.CurrencyCode;

@Component
public class CurrencyConverterDaoImpl implements CurrencyConverterDao {

	@Autowired
	private CurrencyRepository currencyRepository;

	@Override
	public CurrencyEntity findCurrencyByCode(final CurrencyCode currencyCode) {
		return currencyRepository.findByCurrencyCode(currencyCode);
	}

	@Override
	public List<CurrencyEntity> findOtherCurrenciesByCode(final CurrencyCode currencyCode) {
		return currencyRepository.findByCurrencyCodeNot(currencyCode);
	}
}
