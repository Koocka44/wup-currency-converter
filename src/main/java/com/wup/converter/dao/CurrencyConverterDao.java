package com.wup.converter.dao;

import java.util.List;

import com.wup.converter.dao.entity.CurrencyEntity;
import com.wup.converter.model.CurrencyCode;

public interface CurrencyConverterDao {

	/**
	* Get the currency of the given currency code.
	*
	* @param  currencyCode the currency code to search for.
	* @return the data of the given currency from the database.
	*/
	CurrencyEntity findCurrencyByCode(CurrencyCode currencyCode);

	/**
	* Get all the currencies except the one with the given currency code.
	*
	* @param  currencyCode the currency code to NOT search for.
	* @return the data of all the currencies except the one with the given currency code from the database.
	*/
	List<CurrencyEntity> findOtherCurrenciesByCode(CurrencyCode currencyCode);
}
