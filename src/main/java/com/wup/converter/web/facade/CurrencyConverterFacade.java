package com.wup.converter.web.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.service.CurrencyConverterService;
import com.wup.converter.web.request.ExchangeRequest;
import com.wup.converter.web.request.RateRequest;

@Component
public class CurrencyConverterFacade {

	@Autowired
	private CurrencyConverterService currencyConverterService;

	@Transactional(readOnly = true)
	public RateResult getRatesByCurrency(final RateRequest request) {
		return currencyConverterService.getRatesByCurrency(request.getCurrencyCode(), request.getRateType());
	}

	@Transactional(readOnly = true)
	public ExchangeResult exchangeCurrency(final ExchangeRequest request) {
		return currencyConverterService.exchangeCurrency(request.getSourceCurrencyCode(), request.getAmount(), request.getTargetCurrencyCode(), request.getRateType());
	}
}
