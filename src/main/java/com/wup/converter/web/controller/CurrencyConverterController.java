package com.wup.converter.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wup.converter.model.ExchangeResult;
import com.wup.converter.model.RateResult;
import com.wup.converter.web.exception.ValidationException;
import com.wup.converter.web.facade.CurrencyConverterFacade;
import com.wup.converter.web.request.ExchangeRequest;
import com.wup.converter.web.request.RateRequest;
import com.wup.converter.web.response.ValidationError;
import com.wup.converter.web.response.ValidationErrorResponse;

@RestController
public class CurrencyConverterController {

	@Autowired
	private CurrencyConverterFacade currencyConverterFacade;

	@GetMapping("/rates")
	public ResponseEntity<RateResult> getRatesByCurrency(@Valid final RateRequest request, final BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			convertBindingResultToErrorResponse(bindingResult);
		}
		return ResponseEntity.ok().body(currencyConverterFacade.getRatesByCurrency(request));
	}

	@GetMapping("/exchange")
	public ResponseEntity<ExchangeResult> exchangeCurrency(@Valid final ExchangeRequest request, final BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			convertBindingResultToErrorResponse(bindingResult);
		}
		return ResponseEntity.ok().body(currencyConverterFacade.exchangeCurrency(request));
	}

	private void convertBindingResultToErrorResponse(final BindingResult bindingResult) {
		List<FieldError> errors = bindingResult.getFieldErrors();
		ValidationException ex = new ValidationException();
		for (FieldError error : errors) {
			ex.addError(new ValidationError(error.getField(), error.getDefaultMessage()));
		}
		throw ex;
	}

	@ExceptionHandler(ValidationException.class)
	public ResponseEntity<ValidationErrorResponse> handleValidationExceptions(final ValidationException ex) {
		return ResponseEntity.badRequest().body(new ValidationErrorResponse(ex.getErrors()));
	}

}
