package com.wup.converter.web.request;

import javax.validation.constraints.NotNull;

import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.RateType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateRequest {

	@NotNull
	private CurrencyCode currencyCode;
	
	@NotNull
	private RateType rateType;
}
