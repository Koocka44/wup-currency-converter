package com.wup.converter.web.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.wup.converter.model.CurrencyCode;
import com.wup.converter.model.RateType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRequest {

	@NotNull
	private CurrencyCode sourceCurrencyCode;
	
	@NotNull
	@Min(1)
	private Double amount;
	
	@NotNull
	private CurrencyCode targetCurrencyCode;
	
	@NotNull
	private RateType rateType;
}
