package com.wup.converter.web.response;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidationErrorResponse {

	private List<ValidationError> errors = new ArrayList<>();
}
