package com.wup.converter.web.exception;

import java.util.ArrayList;
import java.util.List;

import com.wup.converter.web.response.ValidationError;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private List<ValidationError> errors = new ArrayList<>();
	
	public void addError(final ValidationError error) {
		errors.add(error);
	}
}
