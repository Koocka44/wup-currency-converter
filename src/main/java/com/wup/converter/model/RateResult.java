package com.wup.converter.model;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class RateResult {

	private CurrencyCode sourceCurrency;
	private List<CurrencyRate> currencyRate = new ArrayList<>();

}
