package com.wup.converter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ExchangeResult {

	private CurrencyCode sourceCurrencyCode;
	private CurrencyCode targetCurrencyCode;
	private double sourceAmount;
	private double targetAmount;
}
