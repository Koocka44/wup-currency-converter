package com.wup.converter.model;

public enum CurrencyCode {

	HUF, EUR, USD, GBP, CAD
}
