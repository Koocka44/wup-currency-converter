# W.UP Currency Converter #


### Installation Instructions ###

* Prerequisites
    * Tomcat 8
    * MySQL-based DB ( during development MariaDB was used)

* Deployment
    * Create a database in the DBMS
    * If custom values were used in the previous step, open src/main/resources/application.properties and set the following properties:
        * spring.datasource.url - the jdbc url to the database
        * spring.datasource.username - the database username
        * spring.datasource.password - the database password 
    * run ./mvnw clean install in the root folder of the project
    * copy target/wup-currency-converter.war to <tomcat_dir>/webapps
    * start tomcat, the application should deploy successfully.

### Testing ###

* Import W-UP-Currency-Converter-soapui-project.xml into SoapUI ( the project file can be found in this project's root)
* Edit the two example requests so they point to the correct url ( http://<tomcat_host>:<tomcat_port>/wup-currency-converter )
* There are two example requests for the two functions required in the task. Working values are preconfigured, but can be changed for testing.